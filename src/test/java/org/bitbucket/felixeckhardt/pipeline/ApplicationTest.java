package org.bitbucket.felixeckhardt.pipeline;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ApplicationTest {

    @Test
    public void shouldTestSomething() throws Exception {
        // Given
        int number1 = 5;
        int number2 = 3;

        // When
        int result = number1 + number2;

        // Then
        assertThat(result, is(8));
    }

    @Test
    public void shouldTestSomethingAndFail() throws Exception {
        // Given
        int number1 = 5;
        int number2 = 4;

        // When
        int result = number1 + number2;

        // Then
        assertThat(result, is(9));
    }
}